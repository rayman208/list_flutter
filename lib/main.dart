import 'package:flutter/material.dart';
import 'product.dart';

List<Product> products = List<Product>();

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String Name;
  int Amount;
  double Price;
  int _selectedIndex = -1;

  void _addProduct() {
    setState(() {
      products.add(Product(Name, Amount, Price));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        alignment: Alignment.topLeft,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text('Добавьте продукт', style: TextStyle(fontSize: 30)),
            TextField(
              onChanged: (value) => Name = value,
              decoration: new InputDecoration(hintText: 'Название продукта'),
            ),
            TextField(
              onChanged: (value) => Amount = int.parse(value),
              decoration: new InputDecoration(hintText: 'Кол-во продукта'),
            ),
            TextField(
              onChanged: (value) => Price = double.parse(value),
              decoration: new InputDecoration(hintText: 'Цена продукта(руб.)'),
            ),
            RaisedButton(onPressed: _addProduct, child: Text('Добавить')),
            Expanded(child: ListView.builder(
              itemCount: products.length,
              itemBuilder: (BuildContext context, int index) =>
                  ListTile(
                    onTap: () {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Название продукта: '+products[index].Name, style: TextStyle(fontSize: 24)),
                        Text('Кол-во продукта: '+products[index].Amount.toString(), style: TextStyle(fontSize: 16)),
                        Text('Цена продукта(руб.): '+products[index].Price.toString(), style: TextStyle(fontSize: 16)),
                      ],
                    ),
                    selected: index == _selectedIndex,
                    selectedTileColor: Colors.black12,
                  ),

            ))
          ],
        ),
      ),
    );
  }
}
